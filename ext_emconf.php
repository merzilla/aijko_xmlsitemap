<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "aijko_xmlsitemap"
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'AIJKO XML Sitemap',
	'description' => 'Google sitemap implementation which use the realURL cache tables to generate the sitemap',
	'category' => 'fe',
	'author' => 'David Rühr',
	'author_email' => 'david.ruehr@aijko.com',
	'author_company' => 'AIJKO GmbH',
	'shy' => '0',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'extbase' => '6.2.0-6.2.999',
			'fluid' => '6.2.0-6.2.999',
			'typo3' => '6.2.0-6.2.999',
			'realurl' => '1.12.0-1.99.999',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);