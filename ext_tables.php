<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) die ('Access denied.');

// Add static typoscript files
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Static/page/default', 'TYPO3 :: aijko xml sitemap defaults');

// Create new fields for TCA
$tempColumns = Array (
	'tx_aijkoxmlsitemap_priority' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority',
		'config' => array(
			'type' => 'select',
			'items' => array(
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.0', 0),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.1', 1),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.2', 2),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.3', 3),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.4', 4),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.5', 5),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.6', 6),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.7', 7),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.8', 8),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.8', 9),
				array('LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_priority.10', 10),
			)
		)
	),
	'tx_aijkoxmlsitemap_exclude' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:pages.tx_aijkoxmlsitemap_exclude',
		'config' => array(
			'type' => 'check',
		)
	)
);

// TCA - PAGES: Add fields
//\TYPO3\CMS\Core\Utility\GeneralUtility::loadTCA('pages');
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumns, FALSE);
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('pages', 'miscellaneous', 'tx_aijkoxmlsitemap_priority, tx_aijkoxmlsitemap_exclude');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $tempColumns, FALSE);
// Initialize new palette
$GLOBALS['TCA']['pages']['palettes']['aijko_xmlsitemap_palette'] = array('canNotCollapse' => TRUE);
// Add the palette to all types
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'pages',
	',--div--;LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:tab.xml_sitemap;aijko_xmlsitemap_palette,--palette--;LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:palette.title;aijko_xmlsitemap_palette'
);
// Add the fields in palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('pages', 'aijko_xmlsitemap_palette', 'tx_aijkoxmlsitemap_priority, tx_aijkoxmlsitemap_exclude');


// TCA - PAGES_LANGUAGE_OVERLAY: Add fields
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $tempColumns, FALSE);
// Initialize new palette
$GLOBALS['TCA']['pages_language_overlay']['palettes']['aijko_xmlsitemap_palette'] = array('canNotCollapse' => TRUE);
// Add the palette to all types
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'pages_language_overlay',
	',--div--;LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:tab.xml_sitemap;aijko_xmlsitemap_palette,--palette--;LLL:EXT:aijko_xmlsitemap/Resources/Private/Backend/locallang.xlf:palette.title;aijko_xmlsitemap_palette'
);
// Add the fields in palette
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('pages_language_overlay', 'aijko_xmlsitemap_palette', 'tx_aijkoxmlsitemap_priority, tx_aijkoxmlsitemap_exclude');
unset($tempColumn);