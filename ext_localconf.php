<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

if (!defined('TYPO3_MODE')) die ('Access denied.');

$extensionName = str_replace(' ', '', ucwords(str_replace('_', ' ', $_EXTKEY)));
$vendorName = 'Aijko';

// Configure fce plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
	$vendorName . '.' . $_EXTKEY,
	'Sitemap',
	array(
		'SitemapRecord' => 'show'
	),
	array(
		'SitemapRecord' => ''
	)
);

// Register the default Priority and last change date modifiers for page records:
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aijkoXmlSitemap']['SitemapModifier'][$_EXTKEY] = 'Aijko\AijkoXmlsitemap\Modification\Modifier\PageSitemapRecordModifier';

// Adding new fields to the pageOverlayFields
$pagesOverlayfields = &$GLOBALS['TYPO3_CONF_VARS']['FE']['pageOverlayFields'];
$newPagesOverlayfields = 'tx_aijkoxmlsitemap_priority, tx_aijkoxmlsitemap_exclude';
$pagesOverlayfields .= (empty($pagesOverlayfields)) ? $newPagesOverlayfields : ','.$newPagesOverlayfields;