<?php
namespace Aijko\AijkoXmlsitemap\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Sitemap Record Model
 *
 * @package aijko_xmlsitemap
 */
class SitemapRecord extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * This is the realURL url_hash column
	 *
	 * @var string
	 */
	protected $uid;

	/**
	 * @var string
	 */
	protected $originalParams;

	/**
	 * @var string
	 */
	protected $url;

	/**
	 * @var integer
	 */
	protected $pageId;

	/**
	 * @var string
	 */
	protected $lastModification;

	/**
	 * @var float
	 */
	protected $priority;

	/**
	 * @var integer
	 */
	protected $excludeFromSitemap;

	/**
	 * Getter for uid
	 *
	 * @return string
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * Getter for originalParams
	 *
	 * @return string
	 */
	public function getOriginalParams() {
		return $this->originalParams;
	}

	/**
	 * Getter for url
	 *
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * Getter for pageId
	 *
	 * @return integer
	 */
	public function getPageId() {
		return $this->pageId;
	}

	/**
	 * Getter for lastModification
	 *
	 * @return string
	 */
	public function getLastModification() {
		return $this->lastModification;
	}

	/**
	 * Getter for priority
	 *
	 * @return float
	 */
	public function getPriority() {
		return $this->priority;
	}

	/**
	 * Getter for excludeFromSitemap
	 *
	 * @return integer
	 */
	public function getExcludeFromSitemap() {
		return $this->excludeFromSitemap;
	}

	/**
	 * Setter for uid
	 *
	 * @param string $uid
	 * @return void
	 */
	public function setUid($uid) {
		$this->uid = $uid;
	}

	/**
	 * Setter for originalParams
	 *
	 * @param string $originalParams
	 * @return void
	 */
	public function setOriginalParams($originalParams) {
		$this->originalParams = $originalParams;
	}

	/**
	 * Setter for url
	 *
	 * @param string $url
	 * @return void
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * Setter for pageId
	 *
	 * @param string $pageId
	 * @return void
	 */
	public function setPageId($pageId) {
		$this->pageId = $pageId;
	}

	/**
	 * Setter for lastModification
	 *
	 * @param string $lastModification
	 * @return void
	 */
	public function setLastModification($lastModification) {
		$this->lastModification = $lastModification;
	}

	/**
	 * Setter for priority
	 *
	 * @param string $priority
	 * @return float
	 */
	public function setPriority($priority) {
		$this->priority = $priority;
	}

	/**
	 * Setter for excludeFromSitemap
	 *
	 * @param string $excludeFromSitemap
	 * @return float
	 */
	public function setExcludeFromSitemap($excludeFromSitemap) {
		$this->excludeFromSitemap = $excludeFromSitemap;
	}
}