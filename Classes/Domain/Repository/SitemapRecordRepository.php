<?php
namespace Aijko\AijkoXmlsitemap\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Sitemap Record Repository
 *
 * @package aijko_xmlsitemap
 */
class SitemapRecordRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Search and returns all matching records of relURLs urlencodecache table
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult
	 */
	public function findUrlEncodeCacheRecords() {
		$query = $this->createQuery();
		// Because of the TYPO3 mapping, there must be a uid in the returning records. Otherwise there is the same row for each result
		return $query->statement('SELECT *, url_hash as uid FROM tx_realurl_urlencodecache group by content order by content')
			->execute();
	}
}