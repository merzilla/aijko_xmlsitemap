<?php
namespace Aijko\AijkoXmlsitemap\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Prepare url View Helper
 *
 * @package aijko_xmlsitemap
 */
class PrepareUrlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Sets htmlspecialchars and prefixes a url with its host
	 *
	 * @param string url
	 * @return string
	 */
	public function render($url) {
		if ('' == $url) {
			return '';
		}
		$url = htmlspecialchars($url);
		return \TYPO3\CMS\Core\Utility\GeneralUtility::locationHeaderUrl($url);
	}
}