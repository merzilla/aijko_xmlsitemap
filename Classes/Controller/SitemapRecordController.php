<?php
namespace Aijko\AijkoXmlsitemap\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Sitemap Record Controller
 *
 * @package aijko_xmlsitemap
 */
class SitemapRecordController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * @var \Aijko\AijkoXmlsitemap\Service\SitemapService
	 * @inject
	 */
	protected $sitemapService;

	/**
	 * Set template format for index action
	 *
	 * @return void
	 */
	public function initializeShowAction() {
		$this->request->setFormat('xml');
	}

	/**
	 * Collect encoded urls from realURL
	 *
	 * @return void
	 */
	public function showAction() {
		// Search the records
		$sitemapRecords = $this->sitemapService->getSitemapRecords();
		$this->view->assign('urlRecords', $sitemapRecords);
	}
}