<?php
namespace Aijko\AijkoXmlsitemap\Modification\Modifier;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Page sitemp record modifier
 *
 * @package aijko_xmlsitemap
 */
Class PageSitemapRecordModifier implements \Aijko\AijkoXmlsitemap\Modification\Modifier\ModifierInterface {

	/**
	 * @var \TYPO3\CMS\Frontend\Page\PageRepository
	 * @inject
	 */
	protected $pageRepository;

	/**
	 * @var array
	 */
	protected $page;

	/**
	 * Modify the given page sitemap record
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord
	 * @param string $recordType	is PAGE if it is a real page
	 * @param array $getVars	holds realURL getVars configuration with language and typeNum information
	 * @param array $virtualPageConfig	holds realURL configuration about the called virtual plugin page
	 * @return void
	 */
	public function modify(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord, $recordType = '', $getVars = array(), $virtualPageConfig = array()) {
		if (\Aijko\AijkoXmlsitemap\Service\SitemapService::RECORDS_TYPE_PAGE != $recordType) {
			return;
		}
		$this->page = $this->pageRepository->getPage($sitemapRecord->getPageId());
		// Check if localisation
		if (isset($getVars['L']) && 0 < $getVars['L']) {
			$this->page = $GLOBALS['TSFE']->sys_page->getPageOverlay($this->page, (int) $getVars['L']);
		}
		// To the modfications
		$this->modifyExcludePageFromSitemap($sitemapRecord);
		$this->modifyPagePriority($sitemapRecord);
		$this->modifyPageLastChangedDate($sitemapRecord);
	}

	/**
	 * Modify page exclude from sitemap
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @return void
	 */
	protected function modifyExcludePageFromSitemap(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord) {
		$sitemapRecord->setExcludeFromSitemap((boolean) $this->page['tx_aijkoxmlsitemap_exclude']);
	}

	/**
	 * Modify page priority
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @return void
	 */
	protected function modifyPagePriority(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord) {
		if ($this->page && isset($this->page['tx_aijkoxmlsitemap_priority'])) {
			$sitemapRecord->setPriority((float) $this->page['tx_aijkoxmlsitemap_priority']);
		}
	}

	/**
	 * Modify page last change date
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @return void
	 */
	protected function modifyPageLastChangedDate(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord) {
		if ($this->page && isset($this->page['SYS_LASTCHANGED'])) {
			$sitemapRecord->setLastModification($this->page['SYS_LASTCHANGED']);
		}
	}
}