<?php
namespace Aijko\AijkoXmlsitemap\Modification;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Modification Manager
 *
 * @package aijko_xmlsitemap
 */
class ModificationManager implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 * @inject
	 */
	protected $objectManager;

	/**
	 * Holds all registered modifiers
	 *
	 * @var array
	 */
	protected $modifiers = array();

	/**
	 * Constructor
	 *
	 * @param \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager
	 */
	public function __construct(\TYPO3\CMS\Extbase\Object\ObjectManager $objectManager) {
		$this->objectManager = $objectManager;
		// Initialize modifiers
		$this->initializeModifiers();
	}

	/**
	 * Initialize the modificators
	 *
	 * @return void
	 */
	protected function initializeModifiers() {
		// Instantiate sitemap modifier stored in array.
		if (is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aijkoXmlSitemap']['SitemapModifier'])) {
			foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['aijkoXmlSitemap']['SitemapModifier'] as $classRef) {
				$object = \TYPO3\CMS\Core\Utility\GeneralUtility::getUserObj($classRef);
				$userObj = $this->objectManager->get(get_class($object));
				$this->addModifier($userObj);
			}
		}
	}

	/**
	 * Add a modifier
	 *
	 * @param \Aijko\AijkoXmlsitemap\Modification\Modifier\ModifierInterface $object
	 * @return void
	 */
	protected function addModifier(\Aijko\AijkoXmlsitemap\Modification\Modifier\ModifierInterface $object) {
		$this->modifiers[] = $object;
	}

	/**
	 * Starts modifying url records by using the registered services
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @param string $recordType
	 * @param array $virtualPageConfig
	 * @return void
	 */
	public function modifySitemapRecord(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord &$sitemapRecord, $recordType = '', $virtualPageConfig) {
		// Work trough all modifiers and call their modify method
		foreach ($this->modifiers as $modifier) {
			// Call users modifier
			$modifier->modify($sitemapRecord, $recordType, $virtualPageConfig);
		}
	}
}