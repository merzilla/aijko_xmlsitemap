<?php
namespace Aijko\AijkoXmlsitemap\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Real URL service
 *
 * @package aijko_xmlsitemap
 */
class RealUrlService extends \tx_realurl implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * @var array
	 */
	protected $pageInformations = array();

	/**
	 * @var array
	 */
	protected $virtualPageConfig = array();

	/**
	 * @var array
	 */
	protected $getVars = array();

	/**
	 * @var array
	 */
	protected $realUrlConfig = array();

	/**
	 * Initialize needed data
	 */
	public function __construct() {
		$realUrlConfig = $GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'];
		$host = strtolower((string)\TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('HTTP_HOST'));
		if (isset($realUrlConfig[$host])) {
			$this->realUrlConfig = $realUrlConfig[$host];
		}
	}

	/**
	 * Getter for page informations coming from pages table
	 *
	 * @return array
	 */
	public function getPageInformations() {
		return $this->pageInformations;
	}

	/**
	 * Getter for virtual page configuration coming from realURL
	 *
	 * @return array
	 */
	public function getVirtualPageConfig() {
		return $this->virtualPageConfig;
	}

	/**
	 * Getter for getVars coming from realURL
	 * Can include language and typeNum information
	 *
	 * @return array
	 */
	public function getGetVars() {
		return $this->getVars;
	}

	/**
	 * Resets the virtual page configuration
	 *
	 * @return void
	 */
	protected function reset() {
		$this->virtualPageConfig = array();
	}

	/**
	 * Check if records type is page.
	 * This is a part of the realURL Source. If there is no cache record for one or more of the params, the path could
	 * be a dynamical URL. This is not hundred percent fail safe.
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @return boolean
	 */
	public function isPhysicalPage(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord) {
		$this->reset();
		// Get informations about the path. If it founds all path parts in cache, we found a physical page
		$pathInformations = $this->getPathInformations($sitemapRecord);
		if (!isset($pathInformations['row']) && !isset($pathInformations['postVar'])) {
			// Something went wrong
			return FALSE;
		}
		$row = $pathInformations['row'];
		$postVar = $pathInformations['postVar'];
		$this->getVars = $pathInformations['getVars'];
		if (!$postVar || is_array($postVar)) {
			// Is a physical page
			return TRUE;
		}
		// Get the realURL configuration for the parameters that was found in path
		$postVarsConfiguration = $this->getPostVarsConfiguration($row, $postVar);
		if (!$postVarsConfiguration) {
			return FALSE;
		}
		// If possible it will return the exactly data for the configured extension parameters include table name and more
		$virtualPageConfig = $this->findVirtualPageConfig($postVarsConfiguration, $postVar);
		if (is_array($virtualPageConfig)) {
			$this->virtualPageConfig = $virtualPageConfig;
			return FALSE;
		}
		// Return the best matching configuration for path parameters
		$this->virtualPageConfig = $postVarsConfiguration;
		return FALSE;
	}

	/**
	 * Returns path informations and the last physical page in path, which has been found.
	 * To get these data, it checks if the page path is fully cached. If not, then it could be,
	 * that there are some parameters in path.
	 *
	 * @param \Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord
	 * @return array
	 */
	protected function getPathInformations(\Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord $sitemapRecord) {
		// Remove existing path suffix
		$path = preg_replace('/.html$/', '', $sitemapRecord->getUrl());
		// Explode the path in its parts
		$pathParts = \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode('/', $path);
		// Work from outside-in to look up path in cache:
		$postVar = false;
		// Pulling variables of the path parts
		$getVarsResultingFromAnalysis = $this->decodeSpURL_settingPreVars($pathParts, $this->realUrlConfig['preVars']);
		$copyPathParts = $pathParts;
		// Set the charset
		if ($GLOBALS['TYPO3_CONF_VARS']['BE']['forceCharset']) {
			$charset = $GLOBALS['TYPO3_CONF_VARS']['BE']['forceCharset'];
		} else {
			$charset = $GLOBALS['TSFE']->defaultCharSet;
		}
		// Convert the charset for each path part
		foreach ($copyPathParts as $key => $value) {
			$copyPathParts[$key] = $GLOBALS['TSFE']->csConvObj->conv_case($charset, $value, 'toLower');
		}
		while (count($copyPathParts)) {
			// Using pathq1 index!
			list($row) = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
				'tx_realurl_pathcache.*',
				'tx_realurl_pathcache,pages',
				'tx_realurl_pathcache.page_id=pages.uid AND pages.deleted=0' .
				' AND pagepath=' . $GLOBALS['TYPO3_DB']->fullQuoteStr(implode('/', $copyPathParts), 'tx_realurl_pathcache'),
				'', 'expire', '1');

			// This lookup does not include language and MP var since those are supposed to be fully reflected in the built url!
			if (is_array($row)) {
				break;
			}
			// If no row was found, we simply pop off one element of the path and try again until there are no more elements in the array - which means we didn't find a match!
			$postVar = array_pop($copyPathParts);
		}
		return array('row' => $row, 'postVar' => $postVar, 'getVars' => $getVarsResultingFromAnalysis);
	}

	/**
	 *
	 * @param $row
	 * @param $postVar
	 * @return array
	 */
	protected function getPostVarsConfiguration($row, $postVar) {
		// It could be that entry point to a page but it is not in the cache. If we popped
		// any items from path parts, we need to check if they are defined as postSetVars or
		// fixedPostVars on this page. This does not guarantie 100% success. For example,
		// if path to page is /hello/world/how/are/you and hello/world found in cache and
		// there is a postVar 'how' on this page, the check below will not work. But it is still
		// better than nothing.
		$postVars = array();
		if ($row && $postVar) {
			$postVars = $this->getPostVarsFromRealUrlConfig($row['page_id'], 'postVarSets');
			if (!is_array($postVars) || !isset($postVars[$postVar])) {
				// Check fixed
				$postVars = $this->getPostVarsFromRealUrlConfig($row['page_id'], 'fixedPostVars');
			}
		}
		return $postVars;
	}

	/**
	 * Returns the extension table name for given postVars configuration
	 *
	 * @param array $configurationg
	 * @param string $aliasValue
	 * @return string|boolean if nothing found it will return FALSE
	 */
	protected function findVirtualPageConfig($configurationg, $aliasValue) {
		$rootpageId = isset($configurationg['useUniqueCache_conf']['rootpage_id']) ? intval($configurationg['useUniqueCache_conf']['rootpage_id']) : 0;

		// Look up the ID based on input alias value:
		list($row) = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'tx_realurl_uniqalias',
			'value_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($aliasValue, 'tx_realurl_uniqalias') .
			' AND rootpage_id IN (0,' . (int) $rootpageId . ')' .
			' AND field_alias=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configurationg['alias_field'], 'tx_realurl_uniqalias') .
			' AND field_id=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configurationg['id_field'], 'tx_realurl_uniqalias') .
			' AND tablename=' . $GLOBALS['TYPO3_DB']->fullQuoteStr($configurationg['table'], 'tx_realurl_uniqalias') .
			' AND expire=0');
		if (is_array($row)) {
			return $row['value_id'];
		} else {
			return FALSE;
		}
	}

	/**
	 * Returns configuration for a postVarSet (default) based on input page id
	 *
	 * @param integer $pageId Page id
	 * @param string $mainCat Main key in realurl configuration array. Default is "postVarSets" but could be "fixedPostVars"
	 * @return array
	 */
	public function getPostVarsFromRealUrlConfig($pageId, $mainCat = 'postVarSets') {
		$config = array();
		if (empty($this->realUrlConfig)) {
			return $config;
		}
		// Checking if the value is not an array but a pointer to another key:
		if (isset($hostConfiguration[$mainCat][$pageId]) && !is_array($hostConfiguration[$mainCat][$pageId])) {
			$pageId = $hostConfiguration[$mainCat][$pageId];
		}
		// Check if configuration for this page exists
		if (is_array($hostConfiguration[$mainCat][$pageId])) {
			return $hostConfiguration[$mainCat][$pageId];
		}
		// Check if default configuration exists
		if (is_array($hostConfiguration[$mainCat]['_DEFAULT'])) {
			return $hostConfiguration[$mainCat]['_DEFAULT'];
		}
		return array();
	}
}