<?php
namespace Aijko\AijkoXmlsitemap\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 David Rühr <david.ruehr@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/


/**
 * Sitemap service
 *
 * @package aijko_xmlsitemap
 */
class SitemapService implements \TYPO3\CMS\Core\SingletonInterface {

	/**
	 * @const string
	 */
	const RECORDS_TYPE_PAGE = 'PAGE';

	/**
	 * @var \Aijko\AijkoXmlsitemap\Service\RealUrlService
	 * @inject
	 */
	protected $realUrlService;

	/**
	 * @var \Aijko\AijkoXmlsitemap\Domain\Repository\SitemapRecordRepository
	 * @inject
	 */
	protected $sitemapRecordRepository;

	/**
	 * @var \Aijko\AijkoXmlsitemap\Modification\ModificationManager
	 * @inject
	 */
	protected $modificationManager;

	/**
	 * Collect encoded urls from realURL
	 *
	 * @return\TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $sitemapRecords
	 * @throws \Exception
	 */
	public function getSitemapRecords() {
		// Search the records
		$sitemapRecords = $this->sitemapRecordRepository->findUrlEncodeCacheRecords();
		if (!$sitemapRecords || 0 >= $sitemapRecords->count()) {
			$sitemapRecords = $this->callFrontendAndReadSitemapRecordsAgain();
		}
		if (FALSE === $sitemapRecords) {
			throw new \Exception('Cannot fetch any sitemap data.', 1406100116);
		}

		// Use modification manager to modify sitemap records
		foreach ($sitemapRecords as $sitemapRecord) {
			$recordType = '';
			if (TRUE === $this->realUrlService->isPhysicalPage($sitemapRecord)) {
				$recordType = self::RECORDS_TYPE_PAGE;
			}
			$getVars = $this->realUrlService->getGetVars();
			$virtualPageConfig = $this->realUrlService->getVirtualPageConfig();
			$this->modificationManager->modifySitemapRecord($sitemapRecord, $recordType, $getVars, $virtualPageConfig);
		}
		return $sitemapRecords;
	}

	/**
	 * Call home of the frontend and read records from realURL again.
	 * This is nice if cache is cleared on saving a page
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult|FALSE
	 */
	protected function callFrontendAndReadSitemapRecordsAgain() {
		$host = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST');
		$request = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl($host);
		if (!$request) {
			return FALSE;
		}
		$sitemapRecords = $this->sitemapRecordRepository->findUrlEncodeCacheRecords();
		if (!$sitemapRecords || 0 >= $sitemapRecords->count()) {
			return FALSE;
		}
		return $sitemapRecords;
	}
}