#
# Table structure for table 'pages'
#
CREATE TABLE pages (
	tx_aijkoxmlsitemap_priority int(3) DEFAULT '5' NOT NULL,
	tx_aijkoxmlsitemap_exclude tinyint(4) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'pages_language_overlay'
#
CREATE TABLE pages_language_overlay (
	tx_aijkoxmlsitemap_priority int(3) DEFAULT '5' NOT NULL,
	tx_aijkoxmlsitemap_exclude tinyint(4) unsigned DEFAULT '0' NOT NULL,
);