################################################
#   aijko_xmlsitemap plugin settings
#

	sitemap = PAGE
	sitemap {
		typeNum = 2009

		config {
			debug = 0
			disableAllHeaderCode = 1
			additionalHeaders = Content-Type:application/xml
			xhtml_cleaning >
		}
	}
	sitemap.10 = USER
	sitemap.10 {
		userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
		extensionName = AijkoXmlsitemap
		pluginName = Sitemap
		vendorName = Aijko
		view {
			templateRootPath = {$plugin.tx_aijkoxmlsitemap.view.templateRootPath}
			partialRootPath = {$plugin.tx_aijkoxmlsitemap.view.partialRootPath}
			layoutRootPath = {$plugin.tx_aijkoxmlsitemap.view.layoutRootPath}
		}
	}
	plugin {
		tx_aijkoxmlsitemap >
		tx_aijkoxmlsitemap.settings < sitemap.10.settings
	}