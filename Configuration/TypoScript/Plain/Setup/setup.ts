################################################
#   aijko_xmlsitemap plugin settings
#

	config.tx_extbase {
		persistence{
			classes {
				Aijko\AijkoXmlsitemap\Domain\Model\SitemapRecord {
					mapping {
						tableName = tx_realurl_urlencodecache
						columns {
							url_hash.mapOnProperty = uid
							origparams.mapOnProperty = originalParams
							content.mapOnProperty = url
							page_id.mapOnProperty = pageId
							tstamp.mapOnProperty = lastModification
						}
					}
				}
			}
		}
	}