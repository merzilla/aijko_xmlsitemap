################################################
#   aijko_xmlsitemap plugin configuration settings
#

	plugin.tx_aijkoxmlsitemap {

		view {
			# cat=plugin.tx_aijkoxmlsitemap/file; type=string; label=Path to template root (FE)
			templateRootPath = EXT:aijko_xmlsitemap/Resources/Private/Templates/

			# cat=plugin.tx_aijkoxmlsitemap/file; type=string; label=Path to template partials (FE)
			partialRootPath = EXT:aijko_xmlsitemap/Resources/Private/Partials/

			# cat=plugin.tx_aijkoxmlsitemap/file; type=string; label=Path to template layouts (FE)
			layoutRootPath = EXT:aijko_xmlsitemap/Resources/Private/Layouts/
		}
	}